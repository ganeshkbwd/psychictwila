package hnweb.com.psychictwila;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity {
    PermissionUtility putility;
    ArrayList<String> permission_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh);

        putility = new PermissionUtility(this);
        permission_list = new ArrayList<String>();
//        permission_list.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        permission_list.add(android.Manifest.permission.INTERNET);
        permission_list.add(android.Manifest.permission.ACCESS_WIFI_STATE);
        permission_list.add(android.Manifest.permission.ACCESS_NETWORK_STATE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }, 10000);
    }
}
