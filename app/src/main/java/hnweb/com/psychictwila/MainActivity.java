package hnweb.com.psychictwila;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import hnweb.com.psychictwila.adapter.ZodiacGridAdpater;
import me.nereo.multi_image_selector.MultiImageSelector;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher {
    EditText nameET, emailET, dobET, phoneET, categoryET, queET, zodiacET, imgET;
    String name, email, phone, dob, category, que, zodiac;
    private int mYear, mMonth, mDay, mHour, mMinute;
    ListView timeLV;
    GridView gridView;
    String[] items = {"Helps Commitment Issues",
            "Clears Depression/Stress/Anxiety", "Helps Family Concerns", "Health Issues",
            "Career and Business", "Psychic Phone Readings", "Tarot Card Reading", "Chakra Cleansing",
            "Reunites Lovers", "Stop Break-up/Divorce", "Restores Passion", "Stops Affairs/Cheating Spouse"};
    Integer[] zodiac_img = {android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add, android.R.drawable.ic_menu_add};
    private static final int REQUEST_IMAGE = 2;
    protected static final int REQUEST_STORAGE_READ_ACCESS_PERMISSION = 101;
    protected static final int REQUEST_STORAGE_WRITE_ACCESS_PERMISSION = 102;
    private ArrayList<String> mSymSelectPath = new ArrayList<String>();
    Button uploadBTN;
    String[] zodaic_names = {"AQUARIS", "PICES", "ARIES", "TARUS", "GEMINI",
            "CANCER", "LEO", "VIRGO", "LIBRA", "SCORPIO", "SAGATTARIUS", "CAPRICORN"};

    TextInputLayout nameTIL, emailTIL, dobTIL, phoneTIL, categoryTIL, queTIL, zodiacTIL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

    }

    public void init() {
        nameET = (EditText) findViewById(R.id.nameET);
        emailET = (EditText) findViewById(R.id.emailET);
        dobET = (EditText) findViewById(R.id.dobET);
        phoneET = (EditText) findViewById(R.id.phoneET);
        categoryET = (EditText) findViewById(R.id.categoryET);
        queET = (EditText) findViewById(R.id.queET);
        zodiacET = (EditText) findViewById(R.id.zodiacET);
        uploadBTN = (Button) findViewById(R.id.uploadBTN);
        imgET = (EditText) findViewById(R.id.imgET);

        nameTIL = (TextInputLayout) findViewById(R.id.nameTIL);
        emailTIL = (TextInputLayout) findViewById(R.id.emailTIL);
        dobTIL = (TextInputLayout) findViewById(R.id.dobTIL);
        phoneTIL = (TextInputLayout) findViewById(R.id.phoneTIL);
        categoryTIL = (TextInputLayout) findViewById(R.id.categoryTIL);
        queTIL = (TextInputLayout) findViewById(R.id.queTIL);
        zodiacTIL = (TextInputLayout) findViewById(R.id.zodiacTIL);


        nameET.addTextChangedListener(this);
        emailET.addTextChangedListener(this);
        phoneET.addTextChangedListener(this);
        dobET.addTextChangedListener(this);
        zodiacET.addTextChangedListener(this);
        categoryET.addTextChangedListener(this);
        queET.addTextChangedListener(this);

        dobET.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//your code
                    datePicker();
                }
                //  datePicker();

                return false;
            }
        });

        zodiacET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//                    selectZodiac();
                    selectMyZodiac();

                }
                return false;
            }
        });

        categoryET.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
//your code
                    selectCategory();
//                    selectMyZodiac();
                }

                return false;
            }
        });

    }

    public void selectZodiac() {
        final Dialog settingsDialog = new Dialog(MainActivity.this);
        settingsDialog.getWindow().setTitle("Select Plan");
        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.select_zodiac
                , null));
        settingsDialog.setCancelable(true);
        gridView = (GridView) settingsDialog.findViewById(R.id.gridView);
        ZodiacGridAdpater zodiacGridAdpater = new ZodiacGridAdpater(this, zodiac_img);
//        ArrayAdapter<Integer> itemsAdapter =
//                new ArrayAdapter<Integer>(this, R.layout.zodiac_grid,  zodiac_img);
        gridView.setAdapter(zodiacGridAdpater);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                settingsDialog.dismiss();
                zodiacET.setText(items[i]);
            }
        });

//        gridView =
//        AppConstants.plan = "";
//        AppConstants.price = "";
//        getAllPackages(timeLV, settingsDialog, mResultText3);

        settingsDialog.show();
    }

    public void selectMyZodiac() {

        final Dialog settingsDialog = new Dialog(MainActivity.this);
        settingsDialog.getWindow().setTitle("Select Zodiac");
        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.content_plans
                , null));
        settingsDialog.setCancelable(true);
        timeLV = (ListView) settingsDialog.findViewById(R.id.listView);
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, zodaic_names);
        timeLV.setAdapter(itemsAdapter);
        timeLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                settingsDialog.dismiss();
                zodiacET.setError(null);
                zodiacET.setText(zodaic_names[i]);
            }
        });

//        gridView =
//        AppConstants.plan = "";
//        AppConstants.price = "";
//        getAllPackages(timeLV, settingsDialog, mResultText3);

        settingsDialog.show();
    }

    public void selectCategory() {
        final Dialog settingsDialog = new Dialog(MainActivity.this);
        settingsDialog.getWindow().setTitle("Select Category");
        settingsDialog.setContentView(getLayoutInflater().inflate(R.layout.content_plans
                , null));
        settingsDialog.setCancelable(true);
        timeLV = (ListView) settingsDialog.findViewById(R.id.listView);
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        timeLV.setAdapter(itemsAdapter);
        timeLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                settingsDialog.dismiss();
                categoryET.setError(null);
                categoryET.setText(items[i]);
            }
        });

//        gridView =
//        AppConstants.plan = "";
//        AppConstants.price = "";
//        getAllPackages(timeLV, settingsDialog, mResultText3);

        settingsDialog.show();
    }


    void datePicker() {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        dobET.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        dobET.setError(null);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();


    }

    public void getData() {
        name = nameET.getText().toString();
        email = emailET.getText().toString().trim();
        phone = phoneET.getText().toString().trim();
        dob = dobET.getText().toString().trim();
        category = categoryET.getText().toString().trim();
        que = queET.getText().toString().trim();
        zodiac = zodiacET.getText().toString().trim();
    }

    public void validations() {
        getData();
        if (name.equalsIgnoreCase("") && email.equalsIgnoreCase("") && phone.equalsIgnoreCase("")
                && dob.equalsIgnoreCase("") && category.equalsIgnoreCase("") && que.equalsIgnoreCase("") && zodiac.equalsIgnoreCase("")) {

            nameTIL.setError("Please Enter Name");
            nameET.requestFocus();
            emailTIL.setError("Please Enter Email Address");
            phoneTIL.setError("Please Enter Phone Number");
            dobTIL.setError("Please Select Birth-date");
            zodiacTIL.setError("Please select zodiac.");
            categoryTIL.setError("Please Select category");
            queTIL.setError("Please Enter your question");

        } else if (name.equalsIgnoreCase("")) {
            nameTIL.setError("Please Enter Name");
            nameET.requestFocus();
        } else if (email.equalsIgnoreCase("")) {
            emailTIL.setError("Please Enter Valid Email Address");
            emailET.requestFocus();
        } else if (phone.equalsIgnoreCase("")) {
            phoneTIL.setError("Please Enter Phone Number");
            phoneET.requestFocus();
        } else if (dob.equalsIgnoreCase("")) {
            dobTIL.setError("Please Select Birth-date");
            dobET.requestFocus();

        } else if (zodiac.equalsIgnoreCase("")) {
            zodiacTIL.setError("Please enter zodiac");
            zodiacET.requestFocus();

        } else if (category.equalsIgnoreCase("")) {
            categoryTIL.setError("Please Select category");
            categoryET.requestFocus();
        } else if (que.equalsIgnoreCase("")) {
            queTIL.setError("Please Enter your question");
            queET.requestFocus();
        } else {
            //call web service
            if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                if (Patterns.PHONE.matcher(phone).matches()) {
                    //&& phone.length() == 10
                    if (CheckConnectivity.checkInternetConnection(this)){
                        savewebservice(name, email, phone, dob, category, que, zodiac, mSymSelectPath);
                    }

                } else {
                    phoneTIL.setError("Please Enter Phone Number");
                    phoneET.requestFocus();
                }

            } else {
                emailTIL.setError("Please Enter Valid Email Address");
                emailET.requestFocus();
            }

        }


    }

    public void savewebservice(final String name, final String email, final String phone, final String dob, final String category, final String que, String zodiac, ArrayList<String> mSymSelectPath) {
        final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Please wait ....");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        SimpleMultiPartRequest smr = new SimpleMultiPartRequest(Request.Method.POST, "http://designer321.com/designer321.com/johnram/pshyic_twilla_app/index.php/Api/save_user_ask_question",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e("Response", response);

                        try {
                            Log.e("Response", response);
                            JSONObject jobj = new JSONObject(response);
                            String meg_code = jobj.getString("message_code");
                            if (meg_code.equals("1")) {

                                toastDialog("Your Information submitted successfully.");


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Please Check Internet Connection..!!!", Toast.LENGTH_SHORT).show();
            }
        });

        try {
            for (int i = 0; i < mSymSelectPath.size(); i++) {

                smr.addFile("image[" + i + "]", String.valueOf(mSymSelectPath.get(i)));
                System.out.println("Arsh Op" + "img[" + i + "]" + String.valueOf(mSymSelectPath));
            }
        } catch (Exception e) {

        }
        smr.addStringParam("name", name);
        smr.addStringParam("email", email);
        smr.addStringParam("phone_number", phone);
        smr.addStringParam("date_of_birth", dob);
        smr.addStringParam("category", category);
        smr.addStringParam("question", que);
        smr.addStringParam("zodiac", zodiac);
        smr.setShouldCache(false);

//        RetryPolicy policy = new DefaultRetryPolicy(20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        smr.setRetryPolicy(policy);
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            public void run() {
//                progressDialog.dismiss();
//            }
//        }, 20000);

        queue.add(smr);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submitET:
                validations();
//                Toast.makeText(this, "Under development ....", Toast.LENGTH_SHORT).show();
                break;
            case R.id.uploadBTN:
                pickImage();
                break;

        }

    }

    private void pickImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN // Permission was added in API Level 16
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE,
                    getString(R.string.mis_permission_rationale),
                    REQUEST_STORAGE_READ_ACCESS_PERMISSION);
        } else {

            int maxNum = 1;

            MultiImageSelector selector = MultiImageSelector.create(MainActivity.this);

            selector.showCamera(true);
            selector.count(maxNum);

            selector.multi();

            selector.origin(mSymSelectPath);
            selector.start(MainActivity.this, REQUEST_IMAGE);

        }
    }

    private void requestPermission(final String permission, String rationale, final int requestCode) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.mis_permission_dialog_title)
                    .setMessage(rationale)
                    .setPositiveButton(R.string.mis_permission_dialog_ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
                        }
                    })
                    .setNegativeButton(R.string.mis_permission_dialog_cancel, null)
                    .create().show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_READ_ACCESS_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == RESULT_OK) {

                mSymSelectPath = data.getStringArrayListExtra(MultiImageSelector.EXTRA_RESULT);

                StringBuilder sb = new StringBuilder();
                for (String p : mSymSelectPath) {
                    sb.append(p);
                    sb.append("\n");
                }
                int index = sb.toString().lastIndexOf('/');
                imgET.setText(sb.toString().substring(index + 1));
//                }
                //mResultText2
            }
        }
    }


    public void toastDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.context.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                        nameET.setText("");
                        emailET.setText("");
                        phoneET.setText("");
                        dobET.setText("");
                        zodiacET.setText("");
                        categoryET.setText("");
                        queET.setText("");
                        imgET.setText("");
                        mSymSelectPath.clear();
                        dialog.dismiss();

                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable == nameET.getEditableText()) {
            nameTIL.setError(null);
        } else if (editable == emailET.getEditableText()) {
            emailTIL.setError(null);
        } else if (editable == phoneET.getEditableText()) {
            phoneTIL.setError(null);
        } else if (editable == dobET.getEditableText()) {
            dobTIL.setError(null);
        } else if (editable == zodiacET.getEditableText()) {
            zodiacTIL.setError(null);
        } else if (editable == categoryET.getEditableText()) {
            categoryTIL.setError(null);
        } else if (editable == queET.getEditableText()) {
            queTIL.setError(null);
        }

    }
}
