package hnweb.com.psychictwila.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import hnweb.com.psychictwila.R;

/**
 * Created by neha on 1/30/2017.
 */

public class ZodiacGridAdpater extends BaseAdapter {
    Context context;
    Integer[] zodiac_img;


    public ZodiacGridAdpater(Activity activity, Integer[] zodiac_img) {
        this.context = activity;
        this.zodiac_img = zodiac_img;

    }

    @Override
    public int getCount() {
        return zodiac_img.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(context);
            grid = inflater.inflate(R.layout.zodiac_grid, null);

            ImageView imageView = (ImageView) grid.findViewById(R.id.zodiacIV);

            imageView.setImageResource(zodiac_img[i]);
        } else {
            grid = (View) convertView;
        }
        return grid;
    }
}
